import {Component, OnDestroy, OnInit} from "@angular/core";
import { Subscription } from "rxjs";
import { Post } from '../post.model';
import { PostService } from "../post.service";

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
    posts: Post[] = [];
    private postsSub: Subscription;

    // adding the public keyword here add new property here called postService then assigned the postService to it
    constructor(public postService: PostService) {}

    // this method angular will automatically executes it when this component is initiated
    ngOnInit(): void {
        this.postService.getPosts();
        this.postsSub = this.postService.getPostUpdateListener().subscribe((posts: Post[]) => {
            this.posts = posts;
        });
    }

    onDelete(postId: string) {
        this.postService.deletePost(postId);
    }

    ngOnDestroy() {
        this.postsSub.unsubscribe();
    }
}