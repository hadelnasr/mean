import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
import { map } from "rxjs/operators"

import { Post } from "./post.model";
@Injectable({ providedIn: "root" })
export class PostService {
    private posts: Post[] = [];
    private postsUpdated = new Subject<Post[]>();

    constructor(private http: HttpClient) {}

    getPosts() {
        // make sure the response is a json with message as text and posts as Post model array
        this.http
            .get<{message: string; posts: any}>('http://localhost:3002/api/posts')
            // adding an operator between getting and setting the data
            .pipe(map((postData) => {
                return postData.posts.map(post => {
                    return {
                        title: post.title,
                        content: post.content,
                        id: post._id
                    }
                });
            }))
            .subscribe(transformedPosts => {
                // function will be executed when the request succeeded
                this.posts = transformedPosts;
                this.postsUpdated.next([...this.posts]);
            });
    }

    getPostUpdateListener() {
        return this.postsUpdated.asObservable();
    }

    addPost(title: string, content: string) {
        const newPost: Post = {id: null, title: title, content: content};
        this.http.post<{message: string, postId: string}>('http://localhost:3002/api/posts', newPost).subscribe(responseData => {
            const id = responseData.postId;
            newPost.id = id;
            this.posts.push(newPost);
            this.postsUpdated.next([...this.posts]);
        });
    }

    deletePost(postId: string) {
        this.http
            .delete("http://localhost:3002/api/posts/" + postId)
            .subscribe(() => {
                const updatedPosts = this.posts.filter(post => post.id !== postId);
                this.posts = updatedPosts;
                this.postsUpdated.next([...this.posts]);
            });
    }
}