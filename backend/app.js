const express = require('express');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');

const Post = require('./models/post');
const app = express();

const server = '127.0.0.1:27017';
const database = 'node-angular';

// MySQL    =>  Mongo
// DB       =>  DB
// table    =>  collection
// row      =>  document

// should add the string for the connection with mongoose
mongoose.connect(`mongodb://${server}/${database}`, {useNewUrlParser: true}).then(() => {
    console.log(`Connected to the ${server}/${database} database succeeded!`);
}).catch(() => {
    console.log('Connected to the database failed!');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
    // no matter which domain the app is sending the request from, allow it to access our resources
    res.setHeader("Access-Control-Allow-Origin", "*");
    // the incoming request may have this extra headers
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    // control which http verbs are allowed to send requests
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
    next();
});

app.post('/api/posts', (req, res, next) => {
    // .body is a new field added by bodyParser
    const post = new Post({
        title: req.body.title,
        content: req.body.content
    });
    // the collection name will be the plural name of your model lower case
    // so, will be here posts collection in node-angular database
    post.save().then(createdPost => {
        res.status(201).json({
            message: 'Post added successfully',
            postId: createdPost._id
        });
    });
});

// all the requests targeting /posts
app.get('/api/posts', (req, res, next) => {
    Post.find().then((documents) => {
        // it will end the response implicitly
        res.status(200).json({
            message: "Posts fetched successfully!",
            posts: documents
        });
    });
});

app.delete('/api/posts/:id', (req, res, next) => {
    Post.deleteOne({_id: req.params.id}).then(result => {
        res.status(200).json({
            message: 'Post deleted successfully'
        });
    });
});

// exporting the app to be used globally
module.exports = app;